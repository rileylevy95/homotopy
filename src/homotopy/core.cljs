(ns ^:figwheel-hooks homotopy.core
  (:refer-clojure :exclude [ + - * /])
  (:require
   [cljs.pprint :refer [cl-format]]

   [webjunk.generic-math :refer [+ - * / abs exp log pow]]

   [webjunk.bulma :as bulma]
   [webjunk.complex :as complex :refer [i complex cis]]
   [webjunk.dual :as dual :refer [dual tangent]]
   [webjunk.svg :as svg]
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom])
  (:require-macros [webjunk.pseudotag :refer [deftagfn]]))

(defn tex
  ([s] (tex s {}))
  ([s opts]
   [:span
    {:dangerouslySetInnerHTML
     {:__html
      (.renderToString js/katex s (clj->js opts))}}]))

;; define your app data so that it doesn't get over-written on reload
;; (defonce state (atom {:time 0 :s 0}))

(defn get-app-element []
  (gdom/getElement "app"))

(defn fmt [& args] (apply cl-format nil args))

(defn ptify [[x y]] [x y])
(defn interp [x y]
  (fn [t]
    (+ (* (- 1 t) x)
       (* t y))))

(def infty 10)
(deftagfn vline attr [x]
  (svg/line attr [x (- infty)] [x infty]))
(deftagfn hline attr [y]
  (svg/line attr [(- infty) y] [infty y]))

(def grid (for [i (range -1 2)]
            ^{:key i}
            [:<>
             (vline {:class "grid"} i)
             (hline {:class "grid"} i)]))
(def axes [:<>
           (hline {:class "axis"} 0)
           (vline {:class "axis"} 0)])



(deftagfn cubic attr [[x0 y0] [x1 y1] [x2 y2] [x3 y3] & children]
  [:path (assoc attr
                :d (fmt "M ~a,~a C ~a,~a ~a,~a ~a,~a" x0 y0 x1 y1 x2 y2 x3 y3))
   children])

;; bez(a,b) = t ↦ (1-t)a+tb = (1-t t) · (a b)
;; bez(a,b)'(t) = b-a + (1-t)a' + tb' = b-a+bez(a',b')
;;
;; bez(bez(z0,z1),bez(z1,z2)) = (1-t t) · ((1-t t) · (z0 z1), (1-t t) · (z1 z2))
;;                                        +-      -+
;; +-                      -+   +-     -+ | z0  z1 |
;; | bez(z0,z1)  bez(z1,z2) | = | 1-t t | | z1  z2 |
;; +-                      -+   +-     -+ +-      -+
;;
;;bez(z0,z1,z2) =
;;                               +-     -+ +-     -+ +-   -+
;; bez(bez(z0,z1),bez(z1,z2)) =  | 1-t t | | z0 z1 | | 1-t |
;;                               +-     -+ | z1 z2 | |  t  |
;;                                         +-     -+ +-   -+
;; (index tensor from 0)
;; bez(z0...zn) =?
;; T^(i0 i1 ... in) = z_(i0+...in)
;; B = (1-t t)^⊗n
;; bez(z0 ... zn) = T^(i0 ... in) B_(i0 ... in)


(defn fit3-dual [[s0 s1] f]
  (let [N  (interp s0 s1)
        sf  (comp f N)
        [z0 d0] (sf (dual 0 1))
        [z3 d3] (sf (dual 1 1))
        z1 (+ z0 (/ d0 3))
        z2 (- z3 (/ d3 3))]
    [z0 z1 z2 z3]))

(deftagfn cubic-slice attr [[s0 s1] f]
  (apply cubic attr
         (fit3-dual [s0 s1] f)))


(def τ (* Math/PI 2))

(def steps 8)

(defn H [t s]
  ((interp s (cis (* τ s))) t)
  #_((interp ((interp (+ 1 i) (- 1 i)) s) (cis (* τ s))) t)
  #_((interp ((interp (+ -1 i) (- -1 i)) s) (cis (* τ s))) t))

(defn point [[x y]]
  [:circle.point {:cx x :cy y}])

(def setter
  (memoize
   (fn [state key]
     #(swap! state assoc key (-> % .-target .-value js/parseFloat)))))


(defn param [state display key]
  [:div.level.is-mobile [tex (fmt "~a=" display)]
   [:input.input.is-narrow {:type "text" :value (key @state)
                            :on-change (setter state key)}]
   [:input.slider {:type "range" :min 0 :max 1 :step .001 :value (key @state)
                   :on-change (setter state key)}]])

(defn fix-t [H t & {class :class}]
  [:<>
   (for [n (range 0 steps)
         :let [s0 (/ n steps)
               s1 (/ (inc n) steps)]]
     (cubic-slice {:key n :class (str class " " "sdir")}
            [s0 s1] #(H t %)))])

(defn fix-s [H s & {class :class}]
  [:<>
   (for [n (range 0 steps)
         :let [t0 (/ n steps)
               t1 (/ (inc n) steps)]]
     (cubic-slice {:key n :class (str class " " "tdir")}
       [t0 t1] #(H % s)))])

(defn level-fix-s [H]
  [:<>
   (let [N 26]
     (for [n (range (inc N))
           :let [s (/ n N)]]
       ^{:key n}
       [fix-s H s :class "level"]))])

(defn level-fix-t [H]
  [:<>
   (let [N 8]
     (for [n (range (inc N))
           :let [t (/ n N)]]
       ^{:key t}
       [fix-t H t :class "level"]))])

(defn graph [H t s]
  [:<>
   grid axes [level-fix-s H] [level-fix-t H]
   [fix-s H s]
   [fix-t H t]
   (point (H t s))])

(defn H1 [t s]
  ((interp s (cis (* τ s))) t))

(defn H2 [t s]
  ((interp ((interp (+ 1 i) (- 1 i)) s) (cis (* τ s))) t))

(defn H3 [t s]
  ((interp ((interp (+ -1 i) (- -1 i)) s) (cis (* τ s))) t))

(defn H4 [t s]
  (exp ((interp
         (+ 1 (- (/ 1.05 (+ .05 s))))
         (* i τ s))
        t)))

(complex/real (H4 0 1))

(defn homotopy [& _]
  (let [state (atom {:time 0 :s 0})]
    (fn [tex-code H]
      [:div.column.is-narrow.squish
       [tex tex-code {:displayMode true}]
       [bulma/control {:class "box svgish"}
        [param state "t" :time]
        [param state "s" :s]]

       [:div
        [:svg.diagram.svgish
         {:viewBox "-1.3 -1.3 2.6 2.6"
          :preserveAspectRatio "xMidYMid meet"}
         [graph H (:time @state) (:s @state)]]]])))

(defn hello-world []
  [:main.columns.is-centered.is-multiline
   [homotopy
    "
\\begin{aligned}
f(s)&=s+0i \\\\
g(s)&=e^{i\\tau s} \\\\
H(t,s)&= (1-t)f(s)+tg(s)
\\end{aligned}
"
    H1]
   [homotopy
    "
\\begin{aligned}
f(s)&=(1-s)(1+i) + s(1-i) \\\\
g(s)&=e^{i\\tau s} \\\\
H(t,s)&= (1-t)f(s)+tg(s)
\\end{aligned}
"
    H2]
   [homotopy
    "
\\begin{aligned}
f(s)&=(1-s)(-1+i) + s(-1-i) \\\\
g(s)&=e^{i\\tau s} \\\\
H(t,s)&= (1-t)f(s)+tg(s)
\\end{aligned}
"
    H3]

   [homotopy
    "
\\begin{aligned}
f(s)&= \\exp(1-s^{-1})\\\\
g(s)&= e^{i\\tau s} \\\\
H(t,s)&= \\left(f(s)\\right)^{1-t}\\left(g(s)\\right)^t
\\end{aligned}
"
    H4]])

(defn mount [el]
  (rdom/render [hello-world] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (.typeset js/MathJax)
  (mount-app-element)
  ;; optionally touch your state to force rerendering depending on
  ;; your application
  ;; (swap! state update-in [:__figwheel_counter] inc)
  )
